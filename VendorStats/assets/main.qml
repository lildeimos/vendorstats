import bb.cascades 1.2
import bb.cascades.pickers 1.0
import bb.system 1.2

NavigationPane {
    id: navigationPane

    onCreationCompleted: {
        _csvReader.unzipFinished.connect(onCsvFileUnzipped);
        _csvReader.zipAdded.connect(onZipAdded);
        _csvReader.countEnded.connect(onCountEnded);
        _csvReader.valueAdded.connect(onValueAdded);
        _csvReader.valueChanged.connect(onValueChanged);
    }
    
    onPopTransitionEnded: {
        page.destroy();
    }

    attachedObjects: [
        FilePicker {
            id: filePicker
            type: FileType.Picture
            title: qsTr("Select the vendor report ZIP file")
            viewMode: FilePickerViewMode.ListView
            mode: FilePickerMode.Picker
            filter: ["*.zip"]
            directories : ["/accounts/1000/shared/downloads"]
            onFileSelected: {
                listView.maxValue = 0;
                dropHeaders.removeAll();
                listView.dataModel.clear();
                dropHeaders.title = "";
                output.text = "";
                _csvReader.unZip(selectedFiles[0], mainPage.appendNewZip);
            }
        },
        ComponentDefinition {
            id: headerComponent
            Option {
                property string name
                text: name
            }
        }
    ]
    function onZipAdded(csvFile)
    {
        console.log("onZipAdded", csvFile.toString().split("/").pop());
        csvList.text += "\n" + csvFile.toString().split("/").pop();
    }
    
    function onCsvFileUnzipped(csvFile)
    {
        mainPage.csvFile   = csvFile;
        mainPage.csvHeader = _csvReader.getCsvHeader(csvFile);
        // insert Options into DropBox
        for (var prop in mainPage.csvHeader) 
        {
            var newOption = headerComponent.createObject(dropHeaders);
            newOption.name = prop + " - " + mainPage.csvHeader[prop];
            dropHeaders.add(newOption);
        }
        dropHeaders.title = "Sum for";
    }
    
    function onValueAdded(entry)
    {
        var item = {"value": 1 , "name":entry};
        listView.dataModel.insert(item);
    }
    
    function onValueChanged(numberOfRows, name, value)
    {
        output.text = "Tot: " + numberOfRows;
        listView.totValue = numberOfRows;
        if (listView.maxValue < value) listView.maxValue = value;
        var ip = listView.dataModel.find([name]);
        var item = {"value": value , "name":name};
        listView.dataModel.updateItem(ip, item);
    }
    
    function onCountEnded(numberOfRows, maxValue)
    {
        console.log("onCountEnded", numberOfRows);
        output.text = "Tot: " + numberOfRows;
        listView.totValue = numberOfRows;
        listView.maxValue = maxValue;

        activity.running = false;
        activity.visible = false;
        activity.enabled = false;
    }

    Page {
        id: mainPage
        property bool appendNewZip: false
        property string csvFile
        property variant csvHeader
        Container {
            attachedObjects: LayoutUpdateHandler {
                id: mainLayout
            }
            Label {
                id: csvList
                multiline: true
                textStyle.fontSize: FontSize.XSmall
                text: "Open the vendor report zip file, then choose for which field you want to sum. After opening the first zip you can then merge others"
            }
            Container {
                topPadding: 30
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }

                Button {
                    text: "select zip"
                    onClicked: {
                        csvList.text = "";
                        mainPage.appendNewZip = false;
                        filePicker.open();
                    }
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                }
                Button {
                    text: "add zip"
                    enabled: mainPage.csvFile != ""
                    onClicked: {
                        mainPage.appendNewZip = true;
                        filePicker.open();
                    }
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                }
                DropDown {
                    id: dropHeaders
                    onSelectedIndexChanged: {
                        if (selectedIndex<0) return;
                        listView.dataModel.clear();
                        activity.running = true;
                        activity.visible = true;
                        activity.enabled = true;
                        _csvReader.countColumnEntries(mainPage.csvFile, selectedIndex, listView.dataModel);
                    }
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 3.0
                    }
                }

            }
            
            Container {
                topPadding: 20
                bottomPadding: 20
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                Button {
                    maxWidth: 360
                    text: "Value"
                    onClicked: {
                        if ( listView.dataModel.sortingKeys == "value") listView.dataModel.sortedAscending = !listView.dataModel.sortedAscending
                        listView.dataModel.sortingKeys = ["value"]
                    }
                }
                Button {
                    maxWidth: 360
                    text: "Name"
                    onClicked: {
                        if ( listView.dataModel.sortingKeys == "name") listView.dataModel.sortedAscending = !listView.dataModel.sortedAscending
                        listView.dataModel.sortingKeys = ["name"]
                    }
                }
            }
            
            ActivityIndicator {
                id: activity
                visible: false
                running: false
                enabled: false
            }
            
            ListView {
                id: listView
                property alias width: mainLayout.layoutFrame.width
                property int maxValue
                property int totValue
                dataModel: GroupDataModel {
                    sortingKeys: ["name"]
                    grouping: ItemGrouping.None
                }
                listItemComponents: [
                    ListItemComponent {
                        type: "item"
                        Container {
                                id: itemRoot
                                topPadding: 10
                                layout: DockLayout {}
                                Container {
                                    background: Color.Yellow
                                    minWidth: ListItemData.value/itemRoot.ListItem.view.maxValue * itemRoot.ListItem.view.width
                                    minHeight: 80
                                }
                                Container {
                                    layout: StackLayout {
                                        orientation: LayoutOrientation.LeftToRight
                                    }
                                    Label {
                                        minWidth: 180; maxWidth: 180
                                        text: (Math.floor(ListItemData.value/itemRoot.ListItem.view.totValue*100000)/1000) + "%"
                                    }
                                    Label {
                                        minWidth: 180; maxWidth: 180
                                        text: ListItemData.value
                                    }
                                    Label {
                                        text: ListItemData.name
                                    }
                                }
                        }
                    }
                ]
            }
            
            Label {
                id: output
                textStyle.fontWeight: FontWeight.Bold
            }
        }

    }

}

