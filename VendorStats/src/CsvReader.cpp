/*****************************************************************
             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                   Version 2, December 2004

Copyright (C) 2015 Marco Bavagnoli <lil.deimos@gmail.com>

Everyone is permitted to copy and distribute verbatim or modified
copies of this license document, and changing it is allowed as long
as the name is changed.

           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
  TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

 0. You just DO WHAT THE FUCK YOU WANT TO.
******************************************************************/

#include "CsvReader.h"

#include <QVariantMap>
#include <QMap>
#include <QStringList>
#include <QDir>
#include <QFuture>
#include <qtconcurrentrun.h>

using namespace bb::cascades;

CsvReader::CsvReader(QObject* parent) :
    QObject(parent)
{

}

CsvReader::~CsvReader()
{
}


/*
 *  extract zip file into tmp directory. Not error proof
 */
void CsvReader::unZip(QString zipFile, bool mergeToPrevious)
{
    m_addValues = mergeToPrevious;
    zipFile = zipFile.replace("file://","");
    QProcess *myProcess = new QProcess(this);
    bool b = QObject::connect(myProcess, SIGNAL(finished(int, QProcess::ExitStatus)),
                             this, SLOT(onFinished(int, QProcess::ExitStatus)));
    Q_UNUSED(b);
    Q_ASSERT(b);
    b = QObject::connect(myProcess, SIGNAL(error(QProcess::ProcessError)),
                             this, SLOT(onError(QProcess::ProcessError)));
    Q_ASSERT(b);
    myProcess->start("unzip", QStringList()  << "-o" << zipFile << "-d" << QDir::currentPath()+"/tmp");
}

void CsvReader::onError(QProcess::ProcessError)
{
    qDebug() << "Error unzipping";
}

void CsvReader::onFinished(int exitCode, QProcess::ExitStatus exitStatus)
{
    Q_UNUSED(exitCode);
    Q_UNUSED(exitStatus);
    QProcess *myProcess = static_cast<QProcess*>(sender());
    QByteArray so = myProcess->readAllStandardOutput();
    myProcess->deleteLater();

    qDebug() << "unzipped: " << so;

    QRegExp rx(".*inflating:.*(/.*csv).*\\n");
    rx.setMinimal(true);
    if (rx.indexIn(so) != -1) {
        emit zipAdded(rx.cap(1));
        if (!m_addValues) m_csvFile = rx.cap(1);
        else {
            // merge the main csv file to this one. TODO: check if they have the same header
            QFile main(m_csvFile);
            QFile f(rx.cap(1));
            if (!main.open(QIODevice::Append)) { qDebug() << "Can't open " << m_csvFile; return; }
            if (!f.open(QIODevice::ReadOnly)) { qDebug() << "Can't open " << rx.cap(1); return; }
            f.readLine(); // Skip first header line
            while (!f.atEnd()) {
                main.write( f.readLine() );
            }
            f.close();
            main.close();
            f.remove();
        }
        emit unzipFinished(m_csvFile);
    }
}




/*
 *  get the csv headers
 */
QStringList CsvReader::getCsvHeader(QString csvFile)
{
    QFile f(csvFile);
    m_currHeader.clear();
    if (!f.open(QIODevice::ReadOnly)) return m_currHeader;
    m_currHeader = QString(f.readLine()).split(",");
    f.close();

    // remove " from every QString in the list
    for (int n=0; n< m_currHeader.size(); ++n) m_currHeader[n] = m_currHeader[n].trimmed().mid(1,m_currHeader.at(n).length()-2);
    return m_currHeader;
}

void CsvReader::countColumnEntries(QString csvFile, int column, QObject *dataModel)
{
    m_dataModel = qobject_cast<GroupDataModel*>(dataModel);

    m_dataModel->clear();
    m_dataModel->setSortingKeys(QStringList() << "name");
    m_numberOfRows = 0;
    QFuture<void> f1 = QtConcurrent::run(this, &CsvReader::concurrent, csvFile, column);
}

void CsvReader::concurrent(QString csvFile, int column)
{
    QFile f(csvFile);
    QVariantMap ret;
    int maxValue = 0;
    if (!f.open(QIODevice::ReadOnly)) return;

    qDebug() << "Start counting";
    QStringList row;
    row = QString(f.readLine()).split(",");
    if (row.size()-1<column) return;

    bool isDateTime = false;
    if (m_currHeader.at(column).contains("Date/Time")) isDateTime = true;

    // get the values and store them into 'ret'
    while (!f.atEnd()) {
        row = QString(f.readLine()).split(",");
        if (row.size()<column) continue;
        ++m_numberOfRows;
        // remove " from the field or the time from the Date/Time field
        if (isDateTime)
            row[column] = row[column].trimmed().mid(1,row.at(column).length()-14);
        else
            row[column] = row[column].trimmed().mid(1,row.at(column).length()-2);

        // if the value has been already added once, increment its value
        if (ret[row[column]].isValid()) {
            // name is already there
            int newValue = ret[row[column]].toLongLong() + 1;
            ret[row[column]] = newValue;
            if (maxValue < newValue) maxValue = newValue;
//            emit valueChanged(numberOfRows, row[col] , ret[row[col]].toLongLong()); // Too much hassle !
        } else {
            // add the new entry
            ret.insert(row[column], 1);
//            emit valueAdded(row[col]); // Too much hassle !
        }

    }
    f.close();

    // insert data into dataModel
    QMapIterator<QString, QVariant> i(ret);
    QVariantList list;
    while (i.hasNext()) {
        i.next();
        QVariantMap curr;
        curr.insert("name", i.key());
        curr.insert("value", i.value());
        list << curr;
    }
    m_dataModel->insertList(list);

    emit countEnded(m_numberOfRows, maxValue);
    qDebug() << "END counting";
}

