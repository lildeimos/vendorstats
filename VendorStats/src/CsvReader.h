/*****************************************************************
             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                   Version 2, December 2004

Copyright (C) 2015 Marco Bavagnoli <lil.deimos@gmail.com>

Everyone is permitted to copy and distribute verbatim or modified
copies of this license document, and changing it is allowed as long
as the name is changed.

           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
  TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

 0. You just DO WHAT THE FUCK YOU WANT TO.
******************************************************************/

#ifndef CSVREADER_H_
#define CSVREADER_H_

#include <QObject>
#include <QProcess>

#include <bb/cascades/GroupDataModel>

using namespace bb::cascades;

class CsvReader : public QObject
{
    Q_OBJECT

public:
    CsvReader(QObject* parent = NULL);
    virtual ~CsvReader();

    Q_INVOKABLE void unZip(QString zipFile, bool mergeToPrevious);
    Q_INVOKABLE QStringList getCsvHeader(QString csvFile);
    Q_INVOKABLE void countColumnEntries(QString csvFile, int column, QObject *dataModel);

private:
    GroupDataModel *m_dataModel;
    bool m_addValues;
    QString m_csvFile;
    int m_numberOfRows;
    QStringList m_currHeader;
    void concurrent(QString csvFile, int column);

private Q_SLOTS:
    void onError( QProcess::ProcessError error );
    void onFinished(int exitCode, QProcess::ExitStatus exitStatus);

Q_SIGNALS:
    void unzipFinished(const QString csvFile);
    void zipAdded(const QString csvFile);
    void countEnded(const int numberOfRows, const int maxValue);
    void valueAdded(const QString entry);
    void valueChanged(const int numberOfRows, const QString name , const int value);
};

#endif /* CSVREADER_H_ */
